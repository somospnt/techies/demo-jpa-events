package com.somospnt.jpa.event.service;

import static org.hamcrest.core.StringContains.*;
import static org.junit.Assert.*;

import com.somospnt.jpa.event.repository.PersonaRepository;
import org.junit.Test;
import org.junit.Rule;
import org.junit.runner.RunWith;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.test.context.junit4.SpringRunner;
import com.somospnt.jpa.event.domain.Persona;


@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonaServiceTest {

    @Autowired
    private PersonaService personaService;

    @Autowired
    private PersonaRepository personaRepository;
    
    @Autowired
    private ObjectMapper objectMapper;

    @Rule
    public OutputCapture capture = new OutputCapture();

    @Test
    public void guardar_guardaYEscribeEnElLog() throws JsonProcessingException {
        Persona persona = new Persona();
        persona.setNombre("Steve");
        persona.setApellido("Caballer");
        personaService.guardar(persona);
        assertNotNull(personaRepository.findById(persona.getId()).get());
        assertThat(capture.toString(), containsString(this.objectMapper.writeValueAsString(persona)));
    }

    @Test
    public void actualizar_actualizaYEscribeEnElLog() throws JsonProcessingException {
        Persona persona = new Persona();
        persona.setId(1L);
        persona.setNombre("Tony");
        persona.setApellido("Hawk");
        personaService.actualizar(persona);
        assertNotNull(personaRepository.findById(persona.getId()).get().getApellido(), containsString("Hawk"));
        assertThat(capture.toString(), containsString(this.objectMapper.writeValueAsString(persona)));
    }

}
