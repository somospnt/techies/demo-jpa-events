package com.somospnt.jpa.event.service;

import com.somospnt.jpa.event.domain.Persona;
import com.somospnt.jpa.event.repository.PersonaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PersonaService {
    
    private final PersonaRepository personaRepository;
    
    public void guardar(Persona persona) {
        personaRepository.save(persona);
    }

    public void actualizar(Persona persona) {
        personaRepository.save(persona);
    }
}
