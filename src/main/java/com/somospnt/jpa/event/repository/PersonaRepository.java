package com.somospnt.jpa.event.repository;

import com.somospnt.jpa.event.domain.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaRepository extends JpaRepository<Persona, Long> {
}
