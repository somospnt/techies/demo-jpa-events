package com.somospnt.jpa.event.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.somospnt.jpa.event.domain.Persona;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class PersonaEventListener {

    private final ObjectMapper objectMapper;

    @PostPersist
    public void cuandoGuarda(Persona persona) {
        log.info(toJsonString(persona));
    }

    @PostUpdate
    public void cuandoActualiza(Persona persona) {
        log.info(toJsonString(persona));
    }

    private String toJsonString(Persona persona) {
        try {
            return this.objectMapper.writeValueAsString(persona);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(PersonaEventListener.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

}
